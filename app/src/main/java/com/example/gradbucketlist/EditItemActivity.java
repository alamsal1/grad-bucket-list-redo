package com.example.gradbucketlist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditItemActivity extends AppCompatActivity {
    private static final String EDIT_BUCKET_ITEM_KEY = "EDIT_BUCKET_ITEM";
    private static final String EDIT_BUCKET_ITEM_POSITION_KEY = "EDIT_BUCKET_ITEM_POSITION";
    private BucketItem selectedData;
    private int editItemPosition = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        selectedData = (BucketItem) getIntent().getSerializableExtra(EDIT_BUCKET_ITEM_KEY);
        editItemPosition = getIntent().getIntExtra(EDIT_BUCKET_ITEM_POSITION_KEY, -1);
        setupEditControls(selectedData);

    }

    private void setupEditControls(BucketItem selectedData) {
        EditText etEditItemName = (EditText) findViewById(R.id.Edit_Name);
        EditText etEditItemDescription = (EditText) findViewById(R.id.Edit_Description);
        EditText etEditItemLatitude = (EditText) findViewById(R.id.Edit_Lat);
        EditText etEditItemLongitude = (EditText) findViewById(R.id.Edit_Lon);
        EditText etEditItemDate = (EditText) findViewById(R.id.Edit_Date);
        Boolean validationError = false;

        if (etEditItemLatitude.getText().toString().trim().equals("")){
            etEditItemLatitude.setError("Lat cannot be empty");
            validationError = true;
        }
        if (etEditItemLongitude.getText().toString().trim().equals("")){
            etEditItemLongitude.setError("Long cannot be empty");
            validationError = true;
        }

        if (etEditItemName.getText().toString().trim().equals("")){
            etEditItemName.setError("Task cannot be empty");
            validationError = true;
        }
        if (etEditItemDescription .getText().toString().trim().equals("")){
            etEditItemDescription .setError("Description cannot be empty");
            validationError = true;
        }
        if (etEditItemDate .getText().toString().trim().equals("")){
            etEditItemDate .setError("Date cannot be empty");
            validationError = true;
        }

        if (validationError) return;







        //Set selected item values to the controls for edit
        etEditItemName.setText(selectedData.Name);
        etEditItemDescription.setText(selectedData.Description);

        //set lat and Long
        etEditItemLatitude.setText(String.valueOf(selectedData.Latitude));
        etEditItemLongitude.setText(String.valueOf(selectedData.Longitude));

        //Covert date to preffered format.
        String pattern = "MM/dd/yyyy";
        DateFormat df = new SimpleDateFormat(pattern);
        etEditItemDate.setText(df.format(selectedData.DueDate));
    }


    public void onSaveItem(View v) {
        EditText etNewItemName = (EditText) findViewById(R.id.Edit_Name);
        EditText etNewItemDescription = (EditText) findViewById(R.id.Edit_Description);
        EditText etNewItemLatitude = (EditText) findViewById(R.id.Edit_Lat);
        EditText etNewItemLongitude = (EditText) findViewById(R.id.Edit_Lon);
        EditText etNewItemDate = (EditText) findViewById(R.id.Edit_Date);
        //     Intent intent=new Intent(MainActivity.this,EditItemActivity.class);
        //startActivityForResult(intent, REQUESTCODE_ADD_BUCKETITEM);

        String newName = etNewItemName.getText().toString();
        String newDescription = etNewItemDescription.getText().toString();
        Integer newLat = Integer.valueOf(etNewItemLatitude.getText().toString());
        Integer newLon = Integer.valueOf(etNewItemLongitude.getText().toString());
        String inputDate =  etNewItemDate.getText().toString();
        Date newDueDate = new Date();
        try {

            android.icu.text.SimpleDateFormat formatter = new android.icu.text.SimpleDateFormat("dd/MM/yyyy");
            newDueDate = formatter.parse(inputDate);
        }
        catch (Exception ex){
            // Show error message
            etNewItemDate.setError("Invalid Date!");
            Toast.makeText(this,"Something wrong with DueDate.", Toast.LENGTH_SHORT).show();
        }

        BucketItem updatedItem = new BucketItem( newName, newDescription,newLat, newLon, newDueDate,false);


        Bundle bundle = new Bundle();
        bundle.putSerializable(EDIT_BUCKET_ITEM_KEY, updatedItem);
        bundle.putInt(EDIT_BUCKET_ITEM_POSITION_KEY, editItemPosition);
        Intent intent=new Intent();
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK,intent);
        finish();//finishing activity
    }
}
