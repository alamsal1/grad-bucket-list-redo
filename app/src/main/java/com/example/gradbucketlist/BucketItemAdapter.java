package com.example.gradbucketlist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BucketItemAdapter extends RecyclerView.Adapter<BucketItemAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BucketItem> bucketItems;
    public OnBindCallback onBind;





    public BucketItemAdapter(Context context, ArrayList<BucketItem> bucketItems) {
        this.context = context;
        this.bucketItems = bucketItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bucket_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        //this is the important one to confire.. here's where we grab the list item and populate the view for that position
        if (onBind != null) {
            onBind.onViewBound(holder, position);
        }


        //TODO: add onclicklistener to the nameTextView -> startActivityforResult

}

    @Override
    public int getItemCount() {
        return this.bucketItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public CheckBox nameCheckBox;
        public TextView nameTextView;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameCheckBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            nameTextView = (TextView) itemView.findViewById(R.id.textBucketItem_Name);

        }
    }

    public interface OnBindCallback {
        void onViewBound(ViewHolder viewHolder, int position);
    }
}
