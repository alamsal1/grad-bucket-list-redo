package com.example.gradbucketlist;

import java.io.Serializable;
import java.util.Date;

public class BucketItem implements Serializable {
    public String Name ;
    public String Description ;
    public int Latitude;
    public int Longitude;
    public Date DueDate;
    public boolean Status;

    public BucketItem(String name, String description, int latitude, int longitude, Date dueDate, boolean status )
    {
        this.Name = name;
        this.Description = description;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.DueDate = dueDate;
        this.Status = status;
    }

    public String toString(){return this.Name;}
}
