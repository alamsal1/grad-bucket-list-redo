package com.example.gradbucketlist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;

import java.io.Serializable;
import java.util.Date;

public class AddItemActivity extends AppCompatActivity {

    private static final int REQUESTCODE_ADD_BUCKETITEM = 1;
    private static final String ADD_BUCKET_ITEM_KEY = "ADD_BUCKET_ITEM";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
    }

    public void onAddItem(View view) {
        EditText etNewItemName = (EditText) findViewById(R.id.editText_Name);
        EditText etNewItemDescription = (EditText) findViewById(R.id.editText_Description);
        EditText etNewItemLatitude = (EditText) findViewById(R.id.editText_Lat);
        EditText etNewItemLongitude = (EditText) findViewById(R.id.editText_Lon);
        EditText etNewItemDate = (EditText) findViewById(R.id.editText_Date);

        Boolean validationError = false;

        if (etNewItemLatitude.getText().toString().trim().equals("")){
            etNewItemLatitude.setError("Lat cannot be empty");
            validationError = true;
        }
        if (etNewItemLongitude.getText().toString().trim().equals("")){
            etNewItemLongitude.setError("Long cannot be empty");
            validationError = true;
        }

        if (etNewItemName.getText().toString().trim().equals("")){
            etNewItemName.setError("Task cannot be empty");
            validationError = true;
        }
        if (etNewItemDescription .getText().toString().trim().equals("")){
            etNewItemDescription .setError("Description cannot be empty");
            validationError = true;
        }
        if (etNewItemDate .getText().toString().trim().equals("")){
            etNewItemDate .setError("Date cannot be empty");
            validationError = true;
        }

        if (validationError) return;




        String newName = etNewItemName.getText().toString();
        String newDescription = etNewItemDescription.getText().toString();
        Integer newLat = Integer.valueOf(etNewItemLatitude.getText().toString());
        Integer newLon = Integer.valueOf(etNewItemLongitude.getText().toString());
        String inputDate =  etNewItemDate.getText().toString();
        Date newDueDate = new Date();
        try {

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            newDueDate = formatter.parse(inputDate);
        }
        catch (Exception ex){
            // Show error message
        }

        BucketItem newItem = new BucketItem( newName, newDescription,newLat, newLon, newDueDate,false);


        Bundle bundle = new Bundle();
        bundle.putSerializable(ADD_BUCKET_ITEM_KEY, newItem);
        Intent intent=new Intent();
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK,intent);
        finish();//finishing activity
    }
}
